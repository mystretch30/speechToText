package com.example.demo;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.vosk.LibVosk;
import org.vosk.LogLevel;


/**
 * 实时语音转文本demo
 * 不排除 JpaTransactionManager.class
 *
 * @author YZD
 */
@EnableAsync
@EnableScheduling
@JsonInclude(JsonInclude.Include.NON_NULL)
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class DemoApplication {

    public static void main(String[] args) {

        // 加载vosk语音识别库函数
        LibVosk.setLogLevel(LogLevel.INFO);

        SpringApplication.run(DemoApplication.class, args);
    }

}
